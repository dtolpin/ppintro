# Informal introduction to Probabilistic Programming

* slides/ --- a short presentation with basic ideas
* worksheets/ --- case studies.

Visit https://bitbucket.org/probprog/anglican-user for instructions on running case studies (Gorilla REPL worksheets).
