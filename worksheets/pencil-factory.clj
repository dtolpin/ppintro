;; gorilla-repl.fileformat = 1

;; **
;;; # The "Pencil Factory": a simple beta-binomial example
;;; 
;;; Probabilistic generative models can be written concisely as probabilistic programs. In this
;;; short exercise we will outline the basic structure of a probabilistic program, and show an
;;; example of automatic posterior inference.
;;; 
;;; First we import the necessary libraries to use Anglican,
;; **

;; @@
(ns pencil-factory
  (:require [gorilla-plot.core :as plot]
            [anglican.stat :as stat])
  (:use clojure.repl
        [anglican core runtime emit [state :only [get-predicts]]]))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; **
;;; A classical example problem deals with estimating the failure rate of a process, as in the
;;; following example:
;;; 
;;; >Suppose that you are thinking about purchasing a factory that makes pencils.
;;; Your accountants have determined that you can make a profit (i.e.
;;; you should transact the purchase) if the percentage of defective pencils
;;; manufactured by the factory is less than 30%.
;;; In your prior experience, you learned that, on average, pencil factories produce
;;; defective pencils at a rate of 50%.
;;; To make your judgement about the efficiency of this factory you test pencils
;;; one at a time in sequence as they emerge from the factory to see if they are
;;; defective.
;;; 
;;; We let @@y_1,\ldots,y_N@@, with @@y_n\in\{0,1\}@@ be a set of defective/ not defective observations. A
;;; very simple approach would be to model each observation yn as an independent Bernoulli
;;; trial, with some unknown success rate @@p@@. We place a prior distribution on @@p@@, the shape of
;;; which represents the strength of our conviction that pencil factories produce 50% defective
;;; pencils. A traditional choice of prior might be a uniform distribution on the interval @@[0, 1]@@,
;;; the maximum entropy distribution for @@p@@ which has an expected value of @@0.5@@. In this case,
;;; our full model for the pencil factory data is
;;; 
;;; $$p\sim \mathrm{Uniform}[0,1]$$
;;; $$y_n\sim \mathrm{Bernoulli}(p).$$
;;; 
;;; Suppose the very first pencil that comes off the conveyor belt is defective. We can write this
;;; model as a probabilistic program, complete with observing our defective pencil, as
;; **

;; @@
(defquery run-pencil-factory
    "a simple pencil factory"
     (let [p (sample (uniform-continuous 0 1))]
           (observe (flip p) false)
           (predict :p p)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/run-pencil-factory</span>","value":"#'pencil-factory/run-pencil-factory"}
;; <=

;; **
;;; Now we run the query and plot a histogram of the posterior distribution over p given that we observe one defective pencil.
;; **

;; @@
(def sampler (doquery :smc run-pencil-factory nil :number-of-particles 100))
(def samples (take 10000 (map :p (map get-predicts sampler))))
(plot/histogram samples)


[(float (stat/mean samples))
 (float (stat/variance samples))]
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/sampler</span>","value":"#'pencil-factory/sampler"},{"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/samples</span>","value":"#'pencil-factory/samples"}],"value":"[#'pencil-factory/sampler,#'pencil-factory/samples]"},{"type":"vega","content":{"width":400,"height":247.2187957763672,"padding":{"top":10,"left":55,"bottom":40,"right":10},"data":[{"name":"7cf07c8a-f4d5-4ed6-badf-250118de027d","values":[{"x":0.00011404856208896419,"y":0},{"x":0.0662659531687171,"y":1353},{"x":0.13241785777534523,"y":1164},{"x":0.19856976238197338,"y":1047},{"x":0.2647216669886015,"y":1060},{"x":0.3308735715952296,"y":937},{"x":0.39702547620185774,"y":838},{"x":0.46317738080848586,"y":757},{"x":0.529329285415114,"y":638},{"x":0.5954811900217422,"y":579},{"x":0.6616330946283703,"y":509},{"x":0.7277849992349984,"y":398},{"x":0.7939369038416265,"y":329},{"x":0.8600888084482546,"y":207},{"x":0.9262407130548828,"y":126},{"x":0.9923926176615109,"y":58},{"x":1.058544522268139,"y":0}]}],"marks":[{"type":"line","from":{"data":"7cf07c8a-f4d5-4ed6-badf-250118de027d"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"interpolate":{"value":"step-before"},"fill":{"value":"steelblue"},"fillOpacity":{"value":0.4},"stroke":{"value":"steelblue"},"strokeWidth":{"value":2},"strokeOpacity":{"value":1}}}}],"scales":[{"name":"x","type":"linear","range":"width","zero":false,"domain":{"data":"7cf07c8a-f4d5-4ed6-badf-250118de027d","field":"data.x"}},{"name":"y","type":"linear","range":"height","nice":true,"zero":false,"domain":{"data":"7cf07c8a-f4d5-4ed6-badf-250118de027d","field":"data.y"}}],"axes":[{"type":"x","scale":"x"},{"type":"y","scale":"y"}]},"value":"#gorilla_repl.vega.VegaView{:content {:width 400, :height 247.2188, :padding {:top 10, :left 55, :bottom 40, :right 10}, :data [{:name \"7cf07c8a-f4d5-4ed6-badf-250118de027d\", :values ({:x 1.1404856208896419E-4, :y 0} {:x 0.0662659531687171, :y 1353.0} {:x 0.13241785777534523, :y 1164.0} {:x 0.19856976238197338, :y 1047.0} {:x 0.2647216669886015, :y 1060.0} {:x 0.3308735715952296, :y 937.0} {:x 0.39702547620185774, :y 838.0} {:x 0.46317738080848586, :y 757.0} {:x 0.529329285415114, :y 638.0} {:x 0.5954811900217422, :y 579.0} {:x 0.6616330946283703, :y 509.0} {:x 0.7277849992349984, :y 398.0} {:x 0.7939369038416265, :y 329.0} {:x 0.8600888084482546, :y 207.0} {:x 0.9262407130548828, :y 126.0} {:x 0.9923926176615109, :y 58.0} {:x 1.058544522268139, :y 0})}], :marks [{:type \"line\", :from {:data \"7cf07c8a-f4d5-4ed6-badf-250118de027d\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"7cf07c8a-f4d5-4ed6-badf-250118de027d\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"7cf07c8a-f4d5-4ed6-badf-250118de027d\", :field \"data.y\"}}], :axes [{:type \"x\", :scale \"x\"} {:type \"y\", :scale \"y\"}]}}"}],"value":"[[#'pencil-factory/sampler,#'pencil-factory/samples],#gorilla_repl.vega.VegaView{:content {:width 400, :height 247.2188, :padding {:top 10, :left 55, :bottom 40, :right 10}, :data [{:name \"7cf07c8a-f4d5-4ed6-badf-250118de027d\", :values ({:x 1.1404856208896419E-4, :y 0} {:x 0.0662659531687171, :y 1353.0} {:x 0.13241785777534523, :y 1164.0} {:x 0.19856976238197338, :y 1047.0} {:x 0.2647216669886015, :y 1060.0} {:x 0.3308735715952296, :y 937.0} {:x 0.39702547620185774, :y 838.0} {:x 0.46317738080848586, :y 757.0} {:x 0.529329285415114, :y 638.0} {:x 0.5954811900217422, :y 579.0} {:x 0.6616330946283703, :y 509.0} {:x 0.7277849992349984, :y 398.0} {:x 0.7939369038416265, :y 329.0} {:x 0.8600888084482546, :y 207.0} {:x 0.9262407130548828, :y 126.0} {:x 0.9923926176615109, :y 58.0} {:x 1.058544522268139, :y 0})}], :marks [{:type \"line\", :from {:data \"7cf07c8a-f4d5-4ed6-badf-250118de027d\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"7cf07c8a-f4d5-4ed6-badf-250118de027d\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"7cf07c8a-f4d5-4ed6-badf-250118de027d\", :field \"data.y\"}}], :axes [{:type \"x\", :scale \"x\"} {:type \"y\", :scale \"y\"}]}}]"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-unkown'>0.3311775</span>","value":"0.3311775"},{"type":"html","content":"<span class='clj-unkown'>0.05523852</span>","value":"0.05523852"}],"value":"[0.3311775 0.05523852]"}],"value":"[[[#'pencil-factory/sampler,#'pencil-factory/samples],#gorilla_repl.vega.VegaView{:content {:width 400, :height 247.2188, :padding {:top 10, :left 55, :bottom 40, :right 10}, :data [{:name \"7cf07c8a-f4d5-4ed6-badf-250118de027d\", :values ({:x 1.1404856208896419E-4, :y 0} {:x 0.0662659531687171, :y 1353.0} {:x 0.13241785777534523, :y 1164.0} {:x 0.19856976238197338, :y 1047.0} {:x 0.2647216669886015, :y 1060.0} {:x 0.3308735715952296, :y 937.0} {:x 0.39702547620185774, :y 838.0} {:x 0.46317738080848586, :y 757.0} {:x 0.529329285415114, :y 638.0} {:x 0.5954811900217422, :y 579.0} {:x 0.6616330946283703, :y 509.0} {:x 0.7277849992349984, :y 398.0} {:x 0.7939369038416265, :y 329.0} {:x 0.8600888084482546, :y 207.0} {:x 0.9262407130548828, :y 126.0} {:x 0.9923926176615109, :y 58.0} {:x 1.058544522268139, :y 0})}], :marks [{:type \"line\", :from {:data \"7cf07c8a-f4d5-4ed6-badf-250118de027d\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"7cf07c8a-f4d5-4ed6-badf-250118de027d\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"7cf07c8a-f4d5-4ed6-badf-250118de027d\", :field \"data.y\"}}], :axes [{:type \"x\", :scale \"x\"} {:type \"y\", :scale \"y\"}]}}],[0.3311775 0.05523852]]"}
;; <=

;; **
;;; Recall that a uniform-continuous distribution over the interval [0, 1] is identical to a Beta distribution with pseudocounts a = b = 1. After observing K successes from N trials, we can compute the posterior expectation and variance of p analytically:
;;; 
;;; >E[p] = (a + K) / (a + b + N)
;;; 
;;; >Var[p] = (a + K) * (b + N - K) / (a + b + N)^2 / (a + b + N + 1)
;;; 
;;; We will compare these with the values estimated by averaging over samples from the probabilistic program above (i.e. for a = b = 1, N = 1, K = 0).
;;; 
;; **

;; @@
(defn exp-beta
  "expectation of beta distribution"
  [a b]
  (/ a (+ a b)))

(defn var-beta
  "variance of beta distribution"
  [a b]
  (/ (* a b) (* (Math/pow (+ a b) 2) (+ a b 1))))

(defn exp-beta-pos
  "posterior expectation of beta distribution having observed K successes from N trials"
  [a b N K]
  (exp-beta (+ a K) (- (+ b N) K)))

(defn var-beta-pos
  "posterior variance of beta distribution having observed K successes from N trials"
  [a b N K]
  (var-beta (+ a K) (- (+ b N) K)))
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/exp-beta</span>","value":"#'pencil-factory/exp-beta"},{"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/var-beta</span>","value":"#'pencil-factory/var-beta"}],"value":"[#'pencil-factory/exp-beta,#'pencil-factory/var-beta]"},{"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/exp-beta-pos</span>","value":"#'pencil-factory/exp-beta-pos"}],"value":"[[#'pencil-factory/exp-beta,#'pencil-factory/var-beta],#'pencil-factory/exp-beta-pos]"},{"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/var-beta-pos</span>","value":"#'pencil-factory/var-beta-pos"}],"value":"[[[#'pencil-factory/exp-beta,#'pencil-factory/var-beta],#'pencil-factory/exp-beta-pos],#'pencil-factory/var-beta-pos]"}
;; <=

;; @@
(let [a 1
      b 1
      n 1
      k 0]
  [(float (exp-beta-pos a b n k))
   (float (var-beta-pos a b n k))])
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-unkown'>0.33333334</span>","value":"0.33333334"},{"type":"html","content":"<span class='clj-unkown'>0.055555556</span>","value":"0.055555556"}],"value":"[0.33333334 0.055555556]"}
;; <=

;; **
;;; We see these values closely agree with the ones found empirically.
;;; 
;;; Now we will create a new query that lets us input multiple observations and use a more flexible prior for p,
;; **

;; @@
(defquery run-pencil-factory [a b n k]
    "a simple pencil factory"
     (let [p (sample (beta a b))]
           (observe (binomial n p) k)
           (predict :p p)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/run-pencil-factory</span>","value":"#'pencil-factory/run-pencil-factory"}
;; <=

;; **
;;; This redefines the previous query, but lets us condition on i.i.d Bernoulli samples. Now, introducting 10 true and 3 false pseudocounts, and observing 7 false observations we get,
;; **

;; @@
(def a 10)
(def b 3)
(def n 7)
(def k 0)

(def sampler (doquery :smc run-pencil-factory [a b n k] :number-of-particles 1000))
(def samples (take 10000 (map :p (map get-predicts sampler))))
(plot/histogram samples)


[(float (stat/mean samples))
 (float (stat/variance samples))]

[(float (exp-beta-pos a b n k ))
 (float (var-beta-pos a b n k))]
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/a</span>","value":"#'pencil-factory/a"},{"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/b</span>","value":"#'pencil-factory/b"}],"value":"[#'pencil-factory/a,#'pencil-factory/b]"},{"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/n</span>","value":"#'pencil-factory/n"}],"value":"[[#'pencil-factory/a,#'pencil-factory/b],#'pencil-factory/n]"},{"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/k</span>","value":"#'pencil-factory/k"}],"value":"[[[#'pencil-factory/a,#'pencil-factory/b],#'pencil-factory/n],#'pencil-factory/k]"},{"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/sampler</span>","value":"#'pencil-factory/sampler"}],"value":"[[[[#'pencil-factory/a,#'pencil-factory/b],#'pencil-factory/n],#'pencil-factory/k],#'pencil-factory/sampler]"},{"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/samples</span>","value":"#'pencil-factory/samples"}],"value":"[[[[[#'pencil-factory/a,#'pencil-factory/b],#'pencil-factory/n],#'pencil-factory/k],#'pencil-factory/sampler],#'pencil-factory/samples]"},{"type":"vega","content":{"width":400,"height":247.2187957763672,"padding":{"top":10,"left":55,"bottom":40,"right":10},"data":[{"name":"5101cdc7-81ce-469f-a59c-46fe15f00b0f","values":[{"x":0.30169940429291503,"y":0},{"x":0.33916754340634003,"y":662},{"x":0.37663568251976504,"y":317},{"x":0.41410382163319004,"y":711},{"x":0.45157196074661504,"y":1202},{"x":0.48904009986004005,"y":1226},{"x":0.526508238973465,"y":1430},{"x":0.5639763780868899,"y":1182},{"x":0.6014445172003149,"y":1190},{"x":0.6389126563137398,"y":894},{"x":0.6763807954271648,"y":594},{"x":0.7138489345405897,"y":337},{"x":0.7513170736540147,"y":168},{"x":0.7887852127674396,"y":60},{"x":0.8262533518808646,"y":20},{"x":0.8637214909942895,"y":6},{"x":0.9011896301077145,"y":1},{"x":0.9386577692211394,"y":0}]}],"marks":[{"type":"line","from":{"data":"5101cdc7-81ce-469f-a59c-46fe15f00b0f"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"interpolate":{"value":"step-before"},"fill":{"value":"steelblue"},"fillOpacity":{"value":0.4},"stroke":{"value":"steelblue"},"strokeWidth":{"value":2},"strokeOpacity":{"value":1}}}}],"scales":[{"name":"x","type":"linear","range":"width","zero":false,"domain":{"data":"5101cdc7-81ce-469f-a59c-46fe15f00b0f","field":"data.x"}},{"name":"y","type":"linear","range":"height","nice":true,"zero":false,"domain":{"data":"5101cdc7-81ce-469f-a59c-46fe15f00b0f","field":"data.y"}}],"axes":[{"type":"x","scale":"x"},{"type":"y","scale":"y"}]},"value":"#gorilla_repl.vega.VegaView{:content {:width 400, :height 247.2188, :padding {:top 10, :left 55, :bottom 40, :right 10}, :data [{:name \"5101cdc7-81ce-469f-a59c-46fe15f00b0f\", :values ({:x 0.30169940429291503, :y 0} {:x 0.33916754340634003, :y 662.0} {:x 0.37663568251976504, :y 317.0} {:x 0.41410382163319004, :y 711.0} {:x 0.45157196074661504, :y 1202.0} {:x 0.48904009986004005, :y 1226.0} {:x 0.526508238973465, :y 1430.0} {:x 0.5639763780868899, :y 1182.0} {:x 0.6014445172003149, :y 1190.0} {:x 0.6389126563137398, :y 894.0} {:x 0.6763807954271648, :y 594.0} {:x 0.7138489345405897, :y 337.0} {:x 0.7513170736540147, :y 168.0} {:x 0.7887852127674396, :y 60.0} {:x 0.8262533518808646, :y 20.0} {:x 0.8637214909942895, :y 6.0} {:x 0.9011896301077145, :y 1.0} {:x 0.9386577692211394, :y 0})}], :marks [{:type \"line\", :from {:data \"5101cdc7-81ce-469f-a59c-46fe15f00b0f\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"5101cdc7-81ce-469f-a59c-46fe15f00b0f\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"5101cdc7-81ce-469f-a59c-46fe15f00b0f\", :field \"data.y\"}}], :axes [{:type \"x\", :scale \"x\"} {:type \"y\", :scale \"y\"}]}}"}],"value":"[[[[[[#'pencil-factory/a,#'pencil-factory/b],#'pencil-factory/n],#'pencil-factory/k],#'pencil-factory/sampler],#'pencil-factory/samples],#gorilla_repl.vega.VegaView{:content {:width 400, :height 247.2188, :padding {:top 10, :left 55, :bottom 40, :right 10}, :data [{:name \"5101cdc7-81ce-469f-a59c-46fe15f00b0f\", :values ({:x 0.30169940429291503, :y 0} {:x 0.33916754340634003, :y 662.0} {:x 0.37663568251976504, :y 317.0} {:x 0.41410382163319004, :y 711.0} {:x 0.45157196074661504, :y 1202.0} {:x 0.48904009986004005, :y 1226.0} {:x 0.526508238973465, :y 1430.0} {:x 0.5639763780868899, :y 1182.0} {:x 0.6014445172003149, :y 1190.0} {:x 0.6389126563137398, :y 894.0} {:x 0.6763807954271648, :y 594.0} {:x 0.7138489345405897, :y 337.0} {:x 0.7513170736540147, :y 168.0} {:x 0.7887852127674396, :y 60.0} {:x 0.8262533518808646, :y 20.0} {:x 0.8637214909942895, :y 6.0} {:x 0.9011896301077145, :y 1.0} {:x 0.9386577692211394, :y 0})}], :marks [{:type \"line\", :from {:data \"5101cdc7-81ce-469f-a59c-46fe15f00b0f\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"5101cdc7-81ce-469f-a59c-46fe15f00b0f\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"5101cdc7-81ce-469f-a59c-46fe15f00b0f\", :field \"data.y\"}}], :axes [{:type \"x\", :scale \"x\"} {:type \"y\", :scale \"y\"}]}}]"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-unkown'>0.5145085</span>","value":"0.5145085"},{"type":"html","content":"<span class='clj-unkown'>0.010517462</span>","value":"0.010517462"}],"value":"[0.5145085 0.010517462]"}],"value":"[[[[[[[#'pencil-factory/a,#'pencil-factory/b],#'pencil-factory/n],#'pencil-factory/k],#'pencil-factory/sampler],#'pencil-factory/samples],#gorilla_repl.vega.VegaView{:content {:width 400, :height 247.2188, :padding {:top 10, :left 55, :bottom 40, :right 10}, :data [{:name \"5101cdc7-81ce-469f-a59c-46fe15f00b0f\", :values ({:x 0.30169940429291503, :y 0} {:x 0.33916754340634003, :y 662.0} {:x 0.37663568251976504, :y 317.0} {:x 0.41410382163319004, :y 711.0} {:x 0.45157196074661504, :y 1202.0} {:x 0.48904009986004005, :y 1226.0} {:x 0.526508238973465, :y 1430.0} {:x 0.5639763780868899, :y 1182.0} {:x 0.6014445172003149, :y 1190.0} {:x 0.6389126563137398, :y 894.0} {:x 0.6763807954271648, :y 594.0} {:x 0.7138489345405897, :y 337.0} {:x 0.7513170736540147, :y 168.0} {:x 0.7887852127674396, :y 60.0} {:x 0.8262533518808646, :y 20.0} {:x 0.8637214909942895, :y 6.0} {:x 0.9011896301077145, :y 1.0} {:x 0.9386577692211394, :y 0})}], :marks [{:type \"line\", :from {:data \"5101cdc7-81ce-469f-a59c-46fe15f00b0f\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"5101cdc7-81ce-469f-a59c-46fe15f00b0f\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"5101cdc7-81ce-469f-a59c-46fe15f00b0f\", :field \"data.y\"}}], :axes [{:type \"x\", :scale \"x\"} {:type \"y\", :scale \"y\"}]}}],[0.5145085 0.010517462]]"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-unkown'>0.5</span>","value":"0.5"},{"type":"html","content":"<span class='clj-unkown'>0.011904762</span>","value":"0.011904762"}],"value":"[0.5 0.011904762]"}],"value":"[[[[[[[[#'pencil-factory/a,#'pencil-factory/b],#'pencil-factory/n],#'pencil-factory/k],#'pencil-factory/sampler],#'pencil-factory/samples],#gorilla_repl.vega.VegaView{:content {:width 400, :height 247.2188, :padding {:top 10, :left 55, :bottom 40, :right 10}, :data [{:name \"5101cdc7-81ce-469f-a59c-46fe15f00b0f\", :values ({:x 0.30169940429291503, :y 0} {:x 0.33916754340634003, :y 662.0} {:x 0.37663568251976504, :y 317.0} {:x 0.41410382163319004, :y 711.0} {:x 0.45157196074661504, :y 1202.0} {:x 0.48904009986004005, :y 1226.0} {:x 0.526508238973465, :y 1430.0} {:x 0.5639763780868899, :y 1182.0} {:x 0.6014445172003149, :y 1190.0} {:x 0.6389126563137398, :y 894.0} {:x 0.6763807954271648, :y 594.0} {:x 0.7138489345405897, :y 337.0} {:x 0.7513170736540147, :y 168.0} {:x 0.7887852127674396, :y 60.0} {:x 0.8262533518808646, :y 20.0} {:x 0.8637214909942895, :y 6.0} {:x 0.9011896301077145, :y 1.0} {:x 0.9386577692211394, :y 0})}], :marks [{:type \"line\", :from {:data \"5101cdc7-81ce-469f-a59c-46fe15f00b0f\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"5101cdc7-81ce-469f-a59c-46fe15f00b0f\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"5101cdc7-81ce-469f-a59c-46fe15f00b0f\", :field \"data.y\"}}], :axes [{:type \"x\", :scale \"x\"} {:type \"y\", :scale \"y\"}]}}],[0.5145085 0.010517462]],[0.5 0.011904762]]"}
;; <=

;; **
;;; which as we expect gives p approximately equal to 0.5.
;;; 
;;; Crucially, when writing probabilistic programs we are not limited to simple conjugate models like the one we have defined above. Instead of the beta prior on p, we could introduce a new prior for which we can no longer compute the posterior distribution by hand. For example, suppose we were to place a truncated exponential prior on p, with z ~ exp(2) and p | z = min(z, 1),
;; **

;; @@
(defquery run-pencil-factory [n k]
    "a simple pencil factory"
     (let [p (min 1.0 (sample (exponential 2)))]
       (observe (binomial n p) k)
       (predict :p p)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/run-pencil-factory</span>","value":"#'pencil-factory/run-pencil-factory"}
;; <=

;; @@
(def n 10)
(def k 3)

(def sampler (doquery :smc run-pencil-factory [n k] :number-of-particles 1000))
(def samples (take 10000 (map :p (map get-predicts sampler))))
(plot/histogram samples)


[(float (stat/mean samples))
 (float (stat/variance samples))]

[(float (exp-beta-pos 1 1 n k ))
 (float (var-beta-pos 1 1 n k))]
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/n</span>","value":"#'pencil-factory/n"},{"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/k</span>","value":"#'pencil-factory/k"}],"value":"[#'pencil-factory/n,#'pencil-factory/k]"},{"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/sampler</span>","value":"#'pencil-factory/sampler"}],"value":"[[#'pencil-factory/n,#'pencil-factory/k],#'pencil-factory/sampler]"},{"type":"html","content":"<span class='clj-var'>#&#x27;pencil-factory/samples</span>","value":"#'pencil-factory/samples"}],"value":"[[[#'pencil-factory/n,#'pencil-factory/k],#'pencil-factory/sampler],#'pencil-factory/samples]"},{"type":"vega","content":{"width":400,"height":247.2187957763672,"padding":{"top":10,"left":55,"bottom":40,"right":10},"data":[{"name":"a23a3dd0-cc2f-4266-998d-99698e290890","values":[{"x":0.029703830021449162,"y":0},{"x":0.08412641876906943,"y":177},{"x":0.1385490075166897,"y":621},{"x":0.19297159626430996,"y":1223},{"x":0.24739418501193022,"y":1666},{"x":0.30181677375955046,"y":1622},{"x":0.3562393625071707,"y":1526},{"x":0.41066195125479094,"y":1222},{"x":0.4650845400024112,"y":849},{"x":0.5195071287500315,"y":553},{"x":0.5739297174976518,"y":309},{"x":0.628352306245272,"y":141},{"x":0.6827748949928923,"y":60},{"x":0.7371974837405126,"y":21},{"x":0.7916200724881329,"y":7},{"x":0.8460426612357532,"y":3},{"x":0.9004652499833735,"y":0}]}],"marks":[{"type":"line","from":{"data":"a23a3dd0-cc2f-4266-998d-99698e290890"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"interpolate":{"value":"step-before"},"fill":{"value":"steelblue"},"fillOpacity":{"value":0.4},"stroke":{"value":"steelblue"},"strokeWidth":{"value":2},"strokeOpacity":{"value":1}}}}],"scales":[{"name":"x","type":"linear","range":"width","zero":false,"domain":{"data":"a23a3dd0-cc2f-4266-998d-99698e290890","field":"data.x"}},{"name":"y","type":"linear","range":"height","nice":true,"zero":false,"domain":{"data":"a23a3dd0-cc2f-4266-998d-99698e290890","field":"data.y"}}],"axes":[{"type":"x","scale":"x"},{"type":"y","scale":"y"}]},"value":"#gorilla_repl.vega.VegaView{:content {:width 400, :height 247.2188, :padding {:top 10, :left 55, :bottom 40, :right 10}, :data [{:name \"a23a3dd0-cc2f-4266-998d-99698e290890\", :values ({:x 0.029703830021449162, :y 0} {:x 0.08412641876906943, :y 177.0} {:x 0.1385490075166897, :y 621.0} {:x 0.19297159626430996, :y 1223.0} {:x 0.24739418501193022, :y 1666.0} {:x 0.30181677375955046, :y 1622.0} {:x 0.3562393625071707, :y 1526.0} {:x 0.41066195125479094, :y 1222.0} {:x 0.4650845400024112, :y 849.0} {:x 0.5195071287500315, :y 553.0} {:x 0.5739297174976518, :y 309.0} {:x 0.628352306245272, :y 141.0} {:x 0.6827748949928923, :y 60.0} {:x 0.7371974837405126, :y 21.0} {:x 0.7916200724881329, :y 7.0} {:x 0.8460426612357532, :y 3.0} {:x 0.9004652499833735, :y 0})}], :marks [{:type \"line\", :from {:data \"a23a3dd0-cc2f-4266-998d-99698e290890\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"a23a3dd0-cc2f-4266-998d-99698e290890\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"a23a3dd0-cc2f-4266-998d-99698e290890\", :field \"data.y\"}}], :axes [{:type \"x\", :scale \"x\"} {:type \"y\", :scale \"y\"}]}}"}],"value":"[[[[#'pencil-factory/n,#'pencil-factory/k],#'pencil-factory/sampler],#'pencil-factory/samples],#gorilla_repl.vega.VegaView{:content {:width 400, :height 247.2188, :padding {:top 10, :left 55, :bottom 40, :right 10}, :data [{:name \"a23a3dd0-cc2f-4266-998d-99698e290890\", :values ({:x 0.029703830021449162, :y 0} {:x 0.08412641876906943, :y 177.0} {:x 0.1385490075166897, :y 621.0} {:x 0.19297159626430996, :y 1223.0} {:x 0.24739418501193022, :y 1666.0} {:x 0.30181677375955046, :y 1622.0} {:x 0.3562393625071707, :y 1526.0} {:x 0.41066195125479094, :y 1222.0} {:x 0.4650845400024112, :y 849.0} {:x 0.5195071287500315, :y 553.0} {:x 0.5739297174976518, :y 309.0} {:x 0.628352306245272, :y 141.0} {:x 0.6827748949928923, :y 60.0} {:x 0.7371974837405126, :y 21.0} {:x 0.7916200724881329, :y 7.0} {:x 0.8460426612357532, :y 3.0} {:x 0.9004652499833735, :y 0})}], :marks [{:type \"line\", :from {:data \"a23a3dd0-cc2f-4266-998d-99698e290890\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"a23a3dd0-cc2f-4266-998d-99698e290890\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"a23a3dd0-cc2f-4266-998d-99698e290890\", :field \"data.y\"}}], :axes [{:type \"x\", :scale \"x\"} {:type \"y\", :scale \"y\"}]}}]"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-unkown'>0.3021913</span>","value":"0.3021913"},{"type":"html","content":"<span class='clj-unkown'>0.015575067</span>","value":"0.015575067"}],"value":"[0.3021913 0.015575067]"}],"value":"[[[[[#'pencil-factory/n,#'pencil-factory/k],#'pencil-factory/sampler],#'pencil-factory/samples],#gorilla_repl.vega.VegaView{:content {:width 400, :height 247.2188, :padding {:top 10, :left 55, :bottom 40, :right 10}, :data [{:name \"a23a3dd0-cc2f-4266-998d-99698e290890\", :values ({:x 0.029703830021449162, :y 0} {:x 0.08412641876906943, :y 177.0} {:x 0.1385490075166897, :y 621.0} {:x 0.19297159626430996, :y 1223.0} {:x 0.24739418501193022, :y 1666.0} {:x 0.30181677375955046, :y 1622.0} {:x 0.3562393625071707, :y 1526.0} {:x 0.41066195125479094, :y 1222.0} {:x 0.4650845400024112, :y 849.0} {:x 0.5195071287500315, :y 553.0} {:x 0.5739297174976518, :y 309.0} {:x 0.628352306245272, :y 141.0} {:x 0.6827748949928923, :y 60.0} {:x 0.7371974837405126, :y 21.0} {:x 0.7916200724881329, :y 7.0} {:x 0.8460426612357532, :y 3.0} {:x 0.9004652499833735, :y 0})}], :marks [{:type \"line\", :from {:data \"a23a3dd0-cc2f-4266-998d-99698e290890\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"a23a3dd0-cc2f-4266-998d-99698e290890\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"a23a3dd0-cc2f-4266-998d-99698e290890\", :field \"data.y\"}}], :axes [{:type \"x\", :scale \"x\"} {:type \"y\", :scale \"y\"}]}}],[0.3021913 0.015575067]]"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-unkown'>0.33333334</span>","value":"0.33333334"},{"type":"html","content":"<span class='clj-unkown'>0.017094018</span>","value":"0.017094018"}],"value":"[0.33333334 0.017094018]"}],"value":"[[[[[[#'pencil-factory/n,#'pencil-factory/k],#'pencil-factory/sampler],#'pencil-factory/samples],#gorilla_repl.vega.VegaView{:content {:width 400, :height 247.2188, :padding {:top 10, :left 55, :bottom 40, :right 10}, :data [{:name \"a23a3dd0-cc2f-4266-998d-99698e290890\", :values ({:x 0.029703830021449162, :y 0} {:x 0.08412641876906943, :y 177.0} {:x 0.1385490075166897, :y 621.0} {:x 0.19297159626430996, :y 1223.0} {:x 0.24739418501193022, :y 1666.0} {:x 0.30181677375955046, :y 1622.0} {:x 0.3562393625071707, :y 1526.0} {:x 0.41066195125479094, :y 1222.0} {:x 0.4650845400024112, :y 849.0} {:x 0.5195071287500315, :y 553.0} {:x 0.5739297174976518, :y 309.0} {:x 0.628352306245272, :y 141.0} {:x 0.6827748949928923, :y 60.0} {:x 0.7371974837405126, :y 21.0} {:x 0.7916200724881329, :y 7.0} {:x 0.8460426612357532, :y 3.0} {:x 0.9004652499833735, :y 0})}], :marks [{:type \"line\", :from {:data \"a23a3dd0-cc2f-4266-998d-99698e290890\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"a23a3dd0-cc2f-4266-998d-99698e290890\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"a23a3dd0-cc2f-4266-998d-99698e290890\", :field \"data.y\"}}], :axes [{:type \"x\", :scale \"x\"} {:type \"y\", :scale \"y\"}]}}],[0.3021913 0.015575067]],[0.33333334 0.017094018]]"}
;; <=

;; @@

;; @@
