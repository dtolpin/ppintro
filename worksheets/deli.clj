;; gorilla-repl.fileformat = 1

;; **
;;; Boiler-plate code --- importing necessary things.
;; **

;; @@
(use 'nstools.ns)
(ns+ deli
  (:like anglican-user.worksheet)
  (:require [anglican.stat :refer [mean std] ]))
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"},{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}],"value":"[nil,nil]"}
;; <=

;; **
;;; ## The Deli Dilemma
;;; 
;;; A customer wearing round sunglasses came at 1:13pm, and grabbed a sandwitch and a coffee. Later on the same day, a customer wearing round sunglasses came at 6:09pm and ordered a dinner. Was it the same customer?
;;; 
;;; What we know:
;;; 
;;; * There is an adjacent office quarter, and it takes between 5 and 15 minutes from an office to the deli, varying for different buildings. 
;;; * Depending on traffic lights, the time varies by about 2 minutes.
;;; * The lunch break is at 1:00pm, and the workday ends at 6:00pm.
;;; * The waiter's odds that this is the same customer are 2 to 1.
;;; 
;;; Let's formalize this knowledge (times are in minutes):
;; **

;; @@
(def p-same "prior probability that this is the same customer" (/ 2. 3.))
(def mean-time-to-arrive "average time to arrive from the office quarter" 10)
(def sd-time-to-arrive "standard deviation of arrival time" 3.)
(def time-sd "time deviation" 1)
(def lunch-delay "time between lunch break and lunch order" 13)
(def dinner-delay "time between end of day and dinner order" 9)
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;deli/p-same</span>","value":"#'deli/p-same"},{"type":"html","content":"<span class='clj-var'>#&#x27;deli/mean-time-to-arrive</span>","value":"#'deli/mean-time-to-arrive"}],"value":"[#'deli/p-same,#'deli/mean-time-to-arrive]"},{"type":"html","content":"<span class='clj-var'>#&#x27;deli/sd-time-to-arrive</span>","value":"#'deli/sd-time-to-arrive"}],"value":"[[#'deli/p-same,#'deli/mean-time-to-arrive],#'deli/sd-time-to-arrive]"},{"type":"html","content":"<span class='clj-var'>#&#x27;deli/time-sd</span>","value":"#'deli/time-sd"}],"value":"[[[#'deli/p-same,#'deli/mean-time-to-arrive],#'deli/sd-time-to-arrive],#'deli/time-sd]"},{"type":"html","content":"<span class='clj-var'>#&#x27;deli/lunch-delay</span>","value":"#'deli/lunch-delay"}],"value":"[[[[#'deli/p-same,#'deli/mean-time-to-arrive],#'deli/sd-time-to-arrive],#'deli/time-sd],#'deli/lunch-delay]"},{"type":"html","content":"<span class='clj-var'>#&#x27;deli/dinner-delay</span>","value":"#'deli/dinner-delay"}],"value":"[[[[[#'deli/p-same,#'deli/mean-time-to-arrive],#'deli/sd-time-to-arrive],#'deli/time-sd],#'deli/lunch-delay],#'deli/dinner-delay]"}
;; <=

;; @@
(defquery deli
  (let [time-to-arrive-prior (normal mean-time-to-arrive sd-time-to-arrive)
         same-customer (sample (flip p-same))]
    (predict :same-customer same-customer)
    (if same-customer
      ;; One customer
      (let [time-to-arrive (sample time-to-arrive-prior)]
        (observe (normal time-to-arrive time-sd) lunch-delay)
        (observe (normal time-to-arrive time-sd) dinner-delay)
        (predict :time-to-arrive time-to-arrive))
      ;; Two customers
      (let [time-to-arrive-1 (sample time-to-arrive-prior)
            time-to-arrive-2 (sample time-to-arrive-prior)]
        (observe (normal time-to-arrive-1 time-sd) lunch-delay)
        (observe (normal time-to-arrive-2 time-sd) dinner-delay)
        (predict :times-to-arrive [time-to-arrive-1 time-to-arrive-2])))))

;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;deli/deli</span>","value":"#'deli/deli"}
;; <=

;; **
;;; Now, we lazily perform the inference.
;; **

;; @@
(def samples (doquery :bbvb deli nil))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;deli/samples</span>","value":"#'deli/samples"}
;; <=

;; **
;;; And retrieve predicts from the lazy sequence.
;; **

;; @@
(def N 1000)
(def predicts (map get-predicts (take N (drop N samples))))
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;deli/N</span>","value":"#'deli/N"},{"type":"html","content":"<span class='clj-var'>#&#x27;deli/predicts</span>","value":"#'deli/predicts"}],"value":"[#'deli/N,#'deli/predicts]"}
;; <=

;; **
;;; Let's compute the probability that this is the same customer, and arrival times for each case:
;; **

;; @@
(def p-same+ (/ (count (filter :same-customer predicts)) (double N)))
                            

;; single customer                      
(def time-to-arrive+ (map :time-to-arrive (filter :same-customer predicts)))
(def mean-to-arrive+ (mean time-to-arrive+))
(def sd-to-arrive+ (std time-to-arrive+))

;; two customers
(def times-to-arrive+ (map :times-to-arrive 
                           (filter (complement :same-customer) predicts)))
(def mean-1-to-arrive+ (mean (map first times-to-arrive+)))
(def sd-1-to-arrive+ (std (map first times-to-arrive+)))
(def mean-2-to-arrive+ (mean (map second times-to-arrive+)))
(def sd-2-to-arrive+ (std (map second times-to-arrive+)))
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;deli/p-same+</span>","value":"#'deli/p-same+"},{"type":"html","content":"<span class='clj-var'>#&#x27;deli/time-to-arrive+</span>","value":"#'deli/time-to-arrive+"}],"value":"[#'deli/p-same+,#'deli/time-to-arrive+]"},{"type":"html","content":"<span class='clj-var'>#&#x27;deli/mean-to-arrive+</span>","value":"#'deli/mean-to-arrive+"}],"value":"[[#'deli/p-same+,#'deli/time-to-arrive+],#'deli/mean-to-arrive+]"},{"type":"html","content":"<span class='clj-var'>#&#x27;deli/sd-to-arrive+</span>","value":"#'deli/sd-to-arrive+"}],"value":"[[[#'deli/p-same+,#'deli/time-to-arrive+],#'deli/mean-to-arrive+],#'deli/sd-to-arrive+]"},{"type":"html","content":"<span class='clj-var'>#&#x27;deli/times-to-arrive+</span>","value":"#'deli/times-to-arrive+"}],"value":"[[[[#'deli/p-same+,#'deli/time-to-arrive+],#'deli/mean-to-arrive+],#'deli/sd-to-arrive+],#'deli/times-to-arrive+]"},{"type":"html","content":"<span class='clj-var'>#&#x27;deli/mean-1-to-arrive+</span>","value":"#'deli/mean-1-to-arrive+"}],"value":"[[[[[#'deli/p-same+,#'deli/time-to-arrive+],#'deli/mean-to-arrive+],#'deli/sd-to-arrive+],#'deli/times-to-arrive+],#'deli/mean-1-to-arrive+]"},{"type":"html","content":"<span class='clj-var'>#&#x27;deli/sd-1-to-arrive+</span>","value":"#'deli/sd-1-to-arrive+"}],"value":"[[[[[[#'deli/p-same+,#'deli/time-to-arrive+],#'deli/mean-to-arrive+],#'deli/sd-to-arrive+],#'deli/times-to-arrive+],#'deli/mean-1-to-arrive+],#'deli/sd-1-to-arrive+]"},{"type":"html","content":"<span class='clj-var'>#&#x27;deli/mean-2-to-arrive+</span>","value":"#'deli/mean-2-to-arrive+"}],"value":"[[[[[[[#'deli/p-same+,#'deli/time-to-arrive+],#'deli/mean-to-arrive+],#'deli/sd-to-arrive+],#'deli/times-to-arrive+],#'deli/mean-1-to-arrive+],#'deli/sd-1-to-arrive+],#'deli/mean-2-to-arrive+]"},{"type":"html","content":"<span class='clj-var'>#&#x27;deli/sd-2-to-arrive+</span>","value":"#'deli/sd-2-to-arrive+"}],"value":"[[[[[[[[#'deli/p-same+,#'deli/time-to-arrive+],#'deli/mean-to-arrive+],#'deli/sd-to-arrive+],#'deli/times-to-arrive+],#'deli/mean-1-to-arrive+],#'deli/sd-1-to-arrive+],#'deli/mean-2-to-arrive+],#'deli/sd-2-to-arrive+]"}
;; <=

;; @@
(plot/histogram (map #(if (:same-customer %) 1 2) predicts)
                :bins 4 :x-title (format "number of o customers, p-same=%6g" p-same+))
;; @@
;; =>
;;; {"type":"vega","content":{"width":400,"height":247.2187957763672,"padding":{"top":10,"left":55,"bottom":40,"right":10},"data":[{"name":"10819411-d1a4-4c5c-a417-9a178968712b","values":[{"x":1.0,"y":0},{"x":1.25,"y":218.0},{"x":1.5,"y":0.0},{"x":1.75,"y":0.0},{"x":2.0,"y":0.0},{"x":2.25,"y":782.0},{"x":2.5,"y":0}]}],"marks":[{"type":"line","from":{"data":"10819411-d1a4-4c5c-a417-9a178968712b"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"interpolate":{"value":"step-before"},"fill":{"value":"steelblue"},"fillOpacity":{"value":0.4},"stroke":{"value":"steelblue"},"strokeWidth":{"value":2},"strokeOpacity":{"value":1}}}}],"scales":[{"name":"x","type":"linear","range":"width","zero":false,"domain":{"data":"10819411-d1a4-4c5c-a417-9a178968712b","field":"data.x"}},{"name":"y","type":"linear","range":"height","nice":true,"zero":false,"domain":{"data":"10819411-d1a4-4c5c-a417-9a178968712b","field":"data.y"}}],"axes":[{"type":"x","scale":"x","title":"number of o customers, p-same=0.218000","titleOffset":30},{"type":"y","scale":"y"}]},"value":"#gorilla_repl.vega.VegaView{:content {:width 400, :height 247.2188, :padding {:top 10, :left 55, :bottom 40, :right 10}, :data [{:name \"10819411-d1a4-4c5c-a417-9a178968712b\", :values ({:x 1.0, :y 0} {:x 1.25, :y 218.0} {:x 1.5, :y 0.0} {:x 1.75, :y 0.0} {:x 2.0, :y 0.0} {:x 2.25, :y 782.0} {:x 2.5, :y 0})}], :marks [{:type \"line\", :from {:data \"10819411-d1a4-4c5c-a417-9a178968712b\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"10819411-d1a4-4c5c-a417-9a178968712b\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"10819411-d1a4-4c5c-a417-9a178968712b\", :field \"data.y\"}}], :axes [{:type \"x\", :scale \"x\", :title \"number of o customers, p-same=0.218000\", :titleOffset 30} {:type \"y\", :scale \"y\"}]}}"}
;; <=

;; **
;;; If there is a single customer, there is one arrival time, let's see how it is distributed:
;; **

;; @@
(plot/histogram  time-to-arrive+
                 :x-title (format "arrival time: mean=%6g sd=%6g" 
                                  mean-to-arrive+
                                  sd-to-arrive+))

;; @@
;; =>
;;; {"type":"vega","content":{"width":400,"height":247.2187957763672,"padding":{"top":10,"left":55,"bottom":40,"right":10},"data":[{"name":"01034468-863f-4eb8-82c9-143e5f6cad5d","values":[{"x":9.249103155651985,"y":0},{"x":9.629400449856197,"y":5.0},{"x":10.009697744060409,"y":6.0},{"x":10.38999503826462,"y":18.0},{"x":10.770292332468832,"y":40.0},{"x":11.150589626673044,"y":62.0},{"x":11.530886920877256,"y":44.0},{"x":11.911184215081468,"y":24.0},{"x":12.29148150928568,"y":14.0},{"x":12.671778803489891,"y":5.0},{"x":13.052076097694103,"y":0}]}],"marks":[{"type":"line","from":{"data":"01034468-863f-4eb8-82c9-143e5f6cad5d"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"interpolate":{"value":"step-before"},"fill":{"value":"steelblue"},"fillOpacity":{"value":0.4},"stroke":{"value":"steelblue"},"strokeWidth":{"value":2},"strokeOpacity":{"value":1}}}}],"scales":[{"name":"x","type":"linear","range":"width","zero":false,"domain":{"data":"01034468-863f-4eb8-82c9-143e5f6cad5d","field":"data.x"}},{"name":"y","type":"linear","range":"height","nice":true,"zero":false,"domain":{"data":"01034468-863f-4eb8-82c9-143e5f6cad5d","field":"data.y"}}],"axes":[{"type":"x","scale":"x","title":"arrival time: mean=11.0352 sd=0.619943","titleOffset":30},{"type":"y","scale":"y"}]},"value":"#gorilla_repl.vega.VegaView{:content {:width 400, :height 247.2188, :padding {:top 10, :left 55, :bottom 40, :right 10}, :data [{:name \"01034468-863f-4eb8-82c9-143e5f6cad5d\", :values ({:x 9.249103155651985, :y 0} {:x 9.629400449856197, :y 5.0} {:x 10.009697744060409, :y 6.0} {:x 10.38999503826462, :y 18.0} {:x 10.770292332468832, :y 40.0} {:x 11.150589626673044, :y 62.0} {:x 11.530886920877256, :y 44.0} {:x 11.911184215081468, :y 24.0} {:x 12.29148150928568, :y 14.0} {:x 12.671778803489891, :y 5.0} {:x 13.052076097694103, :y 0})}], :marks [{:type \"line\", :from {:data \"01034468-863f-4eb8-82c9-143e5f6cad5d\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"01034468-863f-4eb8-82c9-143e5f6cad5d\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"01034468-863f-4eb8-82c9-143e5f6cad5d\", :field \"data.y\"}}], :axes [{:type \"x\", :scale \"x\", :title \"arrival time: mean=11.0352 sd=0.619943\", :titleOffset 30} {:type \"y\", :scale \"y\"}]}}"}
;; <=

;; **
;;; For two customers there are two different time distributions, let's compare them.
;; **

;; @@
(plot/compose 
  (plot/histogram (map first times-to-arrive+) 
                  :x-title (format 
                             "arrival times: mean1=%6g, sd1=%6g; mean2=%6g, sd2=%6g" 
                             mean-1-to-arrive+ sd-1-to-arrive+
                             mean-2-to-arrive+ sd-2-to-arrive+)
                  :plot-range [[6 16] :all])
  (plot/histogram (map second times-to-arrive+)))
;; @@
;; =>
;;; {"type":"vega","content":{"width":400,"height":247.2187957763672,"padding":{"top":10,"left":55,"bottom":40,"right":10},"scales":[{"name":"x","type":"linear","range":"width","zero":false,"domain":[6,16]},{"name":"y","type":"linear","range":"height","nice":true,"zero":false,"domain":{"data":"f90ae145-eb0d-4c7a-b320-17259b93686f","field":"data.y"}}],"axes":[{"type":"x","scale":"x","title":"arrival times: mean1=12.6317, sd1=0.916442; mean2=9.10250, sd2=0.931458","titleOffset":30},{"type":"y","scale":"y"}],"data":[{"name":"f90ae145-eb0d-4c7a-b320-17259b93686f","values":[{"x":6.0,"y":0},{"x":6.909090909090909,"y":0.0},{"x":7.818181818181818,"y":0.0},{"x":8.727272727272728,"y":0.0},{"x":9.636363636363638,"y":0.0},{"x":10.545454545454549,"y":11.0},{"x":11.454545454545459,"y":69.0},{"x":12.363636363636369,"y":225.0},{"x":13.272727272727279,"y":276.0},{"x":14.181818181818189,"y":165.0},{"x":15.090909090909099,"y":35.0},{"x":16.000000000000007,"y":1.0},{"x":16.909090909090917,"y":0}]},{"name":"a45f3492-03f1-499a-81fd-e0bf1e08c8b0","values":[{"x":6.024719075409191,"y":0},{"x":6.562785047092767,"y":3.0},{"x":7.100851018776343,"y":10.0},{"x":7.638916990459919,"y":30.0},{"x":8.176982962143494,"y":83.0},{"x":8.71504893382707,"y":142.0},{"x":9.253114905510646,"y":180.0},{"x":9.791180877194222,"y":151.0},{"x":10.329246848877798,"y":108.0},{"x":10.867312820561374,"y":56.0},{"x":11.40537879224495,"y":11.0},{"x":11.943444763928525,"y":7.0},{"x":12.481510735612101,"y":1.0},{"x":13.019576707295677,"y":0}]}],"marks":[{"type":"line","from":{"data":"f90ae145-eb0d-4c7a-b320-17259b93686f"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"interpolate":{"value":"step-before"},"fill":{"value":"steelblue"},"fillOpacity":{"value":0.4},"stroke":{"value":"steelblue"},"strokeWidth":{"value":2},"strokeOpacity":{"value":1}}}},{"type":"line","from":{"data":"a45f3492-03f1-499a-81fd-e0bf1e08c8b0"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"interpolate":{"value":"step-before"},"fill":{"value":"steelblue"},"fillOpacity":{"value":0.4},"stroke":{"value":"steelblue"},"strokeWidth":{"value":2},"strokeOpacity":{"value":1}}}}]},"value":"#gorilla_repl.vega.VegaView{:content {:width 400, :height 247.2188, :padding {:top 10, :left 55, :bottom 40, :right 10}, :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain [6 16]} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"f90ae145-eb0d-4c7a-b320-17259b93686f\", :field \"data.y\"}}], :axes [{:type \"x\", :scale \"x\", :title \"arrival times: mean1=12.6317, sd1=0.916442; mean2=9.10250, sd2=0.931458\", :titleOffset 30} {:type \"y\", :scale \"y\"}], :data ({:name \"f90ae145-eb0d-4c7a-b320-17259b93686f\", :values ({:x 6.0, :y 0} {:x 6.909090909090909, :y 0.0} {:x 7.818181818181818, :y 0.0} {:x 8.727272727272728, :y 0.0} {:x 9.636363636363638, :y 0.0} {:x 10.545454545454549, :y 11.0} {:x 11.454545454545459, :y 69.0} {:x 12.363636363636369, :y 225.0} {:x 13.272727272727279, :y 276.0} {:x 14.181818181818189, :y 165.0} {:x 15.090909090909099, :y 35.0} {:x 16.000000000000007, :y 1.0} {:x 16.909090909090917, :y 0})} {:name \"a45f3492-03f1-499a-81fd-e0bf1e08c8b0\", :values ({:x 6.024719075409191, :y 0} {:x 6.562785047092767, :y 3.0} {:x 7.100851018776343, :y 10.0} {:x 7.638916990459919, :y 30.0} {:x 8.176982962143494, :y 83.0} {:x 8.71504893382707, :y 142.0} {:x 9.253114905510646, :y 180.0} {:x 9.791180877194222, :y 151.0} {:x 10.329246848877798, :y 108.0} {:x 10.867312820561374, :y 56.0} {:x 11.40537879224495, :y 11.0} {:x 11.943444763928525, :y 7.0} {:x 12.481510735612101, :y 1.0} {:x 13.019576707295677, :y 0})}), :marks ({:type \"line\", :from {:data \"f90ae145-eb0d-4c7a-b320-17259b93686f\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}} {:type \"line\", :from {:data \"a45f3492-03f1-499a-81fd-e0bf1e08c8b0\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}})}}"}
;; <=
